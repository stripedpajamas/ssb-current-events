const ssbClient = require('ssb-client');
const ssbKeys = require('ssb-keys');
const ssbFeed = require('ssb-feed');

console.log('starting');
const getSbot = new Promise(((resolve, reject) => {
  ssbClient((err, sbot) => {
    if (err) {
      reject(err);
    }

    resolve(sbot);
  });
}));

getSbot.then((sbot) => {
  console.log('got');
  const keys = ssbKeys.loadOrCreateSync('./app-private.key');
  const alice = ssbFeed(sbot, keys);

  if (true) {
    alice.publish({
      type: 'about',
      about: '@wNmXqk80DL4FrBjzZcYqbKs/SpsPv6MVX6BLICabPfI=.ed25519',
      name: 'Current Events',
    }, (err, msg) => {
      if (err) throw err;
      console.log(msg);
    });
  } else {
    console.log('Did you forget to turn off debug mode?');
  }
});
