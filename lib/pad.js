module.exports = size => (n) => {
  let s = String(n);
  while (s.length < (size || 2)) { s = `0${s}`; }
  return s;
};
