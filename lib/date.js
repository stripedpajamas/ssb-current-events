const pad = require('./pad')(2);

const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December',
];

const timeLeft = (PRODUCTION) => {
  // next run prediction
  const now = new Date();
  now.setDate(now.getDate() - 2);

  const left = {
    hours: pad(24 - now.getUTCHours() - 1),
    minutes: pad(60 - now.getUTCMinutes()),
    seconds: pad(60 - now.getUTCSeconds()),
  };

  if (!PRODUCTION) {
    left.hours = '00';
    left.minutes = '00';
  }

  return left;
};

const getDateData = () => {
  const d = new Date();
  d.setDate(d.getDate() - 2);

  const date = {
    year: d.getUTCFullYear(),
    month: monthNames[d.getUTCMonth()],
    monthNum: d.getUTCMonth() + 1,
    date: d.getUTCDate(),
  };

  const isoDate = {
    year: pad(date.year),
    month: pad(date.monthNum),
    date: pad(date.date),
  };

  const dateString = `${date.year} ${date.month} ${date.date}`;

  const isoDateString = `${isoDate.year}-${isoDate.month}-${isoDate.date}`;

  return {
    date,
    isoDate,
    dateString,
    isoDateString,
  };
};

module.exports = {
  timeLeft,
  getDateData,
};
