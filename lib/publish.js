const ssbClient = require('ssb-client');
const ssbKeys = require('ssb-keys');
const ssbFeed = require('ssb-feed');

const getSbot = () => new Promise(((resolve, reject) => {
  ssbClient((err, sbot) => {
    if (err) {
      reject(err);
    }

    resolve(sbot);
  });
}));

const publish = (text, cb) => {
  getSbot().then((sbot) => {
    const keys = ssbKeys.loadOrCreateSync('./app-private.key');
    const alice = ssbFeed(sbot, keys);
    alice.publish({
      type: 'post',
      channel: 'current-events',
      text,
    }, (err, msg) => {
      if (err) throw err;
      console.log(msg);
      if (cb) cb();
    });
  });
};

module.exports = publish;
