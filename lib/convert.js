const TurndownService = require('turndown');
const jsdom = require('jsdom');

// instantiation
const { JSDOM } = jsdom;
const turndownService = new TurndownService();

// turndown config
turndownService.addRule('absolute-links', {
  filter: 'a',
  replacement(content, node) {
    if (node.href.indexOf('/wiki/') === 0) {
      return `[${content}](https://en.wikipedia.org${node.href})`;
    }
    return `[${content}](${node.href})`;
  },
});

turndownService.addRule('actual-headings', {
  filter: 'div',
  replacement(content) {
    return `## ${content}`;
  },
});

const convert = (html) => {
  const dom = new JSDOM(html);
  const { document } = dom.window;
  const markdown = turndownService.turndown(document.querySelector('.description'));
  return markdown;
};

module.exports = convert;
